	  ************************
	  *   Magicfying Glass	 *
	  ************************

Written by Thomas Huth, 1997

This program is in the public domain, feel free to use the source code as you
like, but it would be nice if you keep a comment that the original version has
been written by me.

This small program is a magnifying glass for your GEM desktop. The screen
contents below the mouse pointer are zoomed and displayed in real-time in the
window of the application. The program can run as both, PRG and ACC, simply
change the extension of the file.

In the configuration dialog, you can choose the zoom level: 2x, 4x or "mega"
(note that the latter has been implemented rather just for fun than being
really useful). You can also select whether the mouse curser should be
enlarged as well, or only the contents under the mouse pointer. The "Wind-
Update" option is used to select whether the program should use wind_update()
GEM calls to lock the screen for doing its updates. This is recommended by
default, but some other programs lock the screen for a long time, so you
don't get any updates in that case. If you're running such other programs,
you should disable the wind_update option instead.

 Enjoy,
  Thomas
